#!/usr/bin/env python3
import openpyxl as xl
import numpy as np
def readxlsx(fname = 'result.xlsx'):
    """อ่าน xlsx จากผลลัพธ์ของ ipynb ของ kriengkrai"""
    data = []
    wb = xl.load_workbook(filename = fname)
    print(wb.sheetnames)
    s = wb['Sheet1']
    c = 'ACBDEFG'
    for i in range(1,21):
        data.append( ([s[e+str(i)].value for e in c]))
    convert2numpy(data)

def convert2numpy(data):
    """แปลง list เป็น list เป็น numpy.ndarray"""
    datanp = np.array(data)
    save(datanp)

def save(datanp):
    np.savetxt('result.txt',datanp,fmt='%s')

def load():
    mm = np.loadtxt('result.txt',dtype=np.str)
    print(mm)
readxlsx()
load()
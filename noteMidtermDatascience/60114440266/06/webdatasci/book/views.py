from django.shortcuts import render
from django.http import HttpResponse
from .models import book
from django.contrib import admin
from django.urls import path
from book import views
from django.contrib.auth import views as auth_views
from django.conf.urls import url
import pandas as pd
import numpy as np

# Create your views here.


def qt(req):
    return render(req, 'book/qt.html')
def index(req):
    booked = book.objects.all()
    #print(type((booked))
    #book1 =book.objects(1)
    return render(req, 'book/index.html', {'booked':booked})

def read_db():
    data = {} 
    labels = [ 'price','name','books','numB_Wr' ,'pub','saleP','type1','type2','writer' ]
    for label in labels:
        data[label] = []
    for b in book.objects.all():
        data['price'].append( b.price )
        data['name'].append( b.name )
        data['books'].append( b.books )
        data['numB_Wr'].append( b.numB_Wr )
        data['pub'].append( b.pub )
        data['saleP'].append( b.saleP )
        data['type1'].append( b.type1 )
        data['type2'].append( b.type2 )
        data['writer'].append( b.writer )
    return pd.DataFrame.from_dict(data)
def price_report(req):
    df = read_db()
    return render(req, 'book/price_report.html', { 
        'dfmax': [ 
                f'{df.price.max():,.2f}',
                f'{df.saleP.max():,.2f}',
                f'{df.numB_Wr.max():,.2f}'
               
                ],
        'dfmean': [ 
                f'{df.price.mean():,.2f}',
                f'{df.saleP.mean():,.2f}',
                f'{df.numB_Wr.mean():,.2f}'

                ],
        'dfmin': [ 
                f'{df.price.min():,.2f}',
                f'{df.saleP.min():,.2f}',
                f'{df.numB_Wr.min():,.2f}'
        ],
        'dfstd': [ 
                f'{df.price.std():,.2f}',
                f'{df.saleP.std():,.2f}',
                f'{df.numB_Wr.std():,.2f}'
                
                ]
        })


def priceM(req):
    df = read_db()
    data = df.groupby(['type1']).mean()
    report = [ ]
    for type1,p in data.iterrows():
        report.append( [ type1, f'{p.price:,.2f}', f'{p.saleP:,.2f}', f'{p.numB_Wr:,.2f}',  f'{p.price/p.numB_Wr:,.2f}' ] )
    return render(req, 'book/priceM.html', { 'report': report})

def pw(req):
    df = read_db()
    data = df.groupby(['pub', 'writer']).mean()
    report = [ ]
    for pw,p in data.iterrows():
        report.append( [ pw[0], pw[1],  f'{p.price:,.2f}', f'{p.saleP:,.2f}', f'{p.numB_Wr:,.2f}',  f'{p.price/p.numB_Wr:,.2f}' ] )
    return render(req, 'book/pw.html', { 'report': report })

def report(req):
    data = []
    x = book.objects.all()
    for detail in x:
        data.append([detail.name,(detail.price)/2])
    op = pd.DataFrame(data,columns=['Nn','Pr'])
    namemy = op.groupby('Nn')
    arg = namemy['Pr'].agg(np.mean)
    sa = arg.sort_values(ascending=False)
    yes = sa.to_dict()
    print(yes)
    return render(req,'book/report.html',{'arg':yes})

# Generated by Django 2.1.5 on 2019-01-30 14:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('book', '0004_auto_20190130_1452'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='price',
            field=models.CharField(default=' ', max_length=100),
        ),
        migrations.AlterField(
            model_name='book',
            name='saleP',
            field=models.CharField(default=' ', max_length=100),
        ),
    ]

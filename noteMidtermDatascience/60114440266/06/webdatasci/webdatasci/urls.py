from django.contrib import admin
from django.urls import path
from book import views,pushdata

urlpatterns = [
    path('', views.index),
    path('index', views.index),
    path('admin/', admin.site.urls),
    path('pushdata/',pushdata.load),
    path('price', views.report),
    path('report', views.price_report),
    path('priceM',views.priceM),
    path('pw',views.pw),
    path('qt', views.qt)
]

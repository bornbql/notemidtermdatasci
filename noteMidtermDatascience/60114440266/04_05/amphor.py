#!/usr/bin/python3
# -- coding: utf-8 --
import pandas as pd

df = pd.read_csv('baans-4000.csv')
nan = df[(df.area > 0) & (df.province=='น่าน')]
diss = nan.groupby('district').mean()
showdt = pd.DataFrame(diss.price/diss.area,columns=['price']).sort_values(by='price',ascending=False)

print(showdt.reset_index())

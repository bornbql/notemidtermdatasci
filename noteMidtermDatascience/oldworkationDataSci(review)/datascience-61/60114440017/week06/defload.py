# -*- coding: utf-8 -*-
"""
นายปิยะวัฒน์ จังอินทร์ 60114440198
ข้อมูลจาก 60114440211
"""
import openpyxl as xl

import numpy as np

def readxlsx(fname = 'SpecLabtop.xlsx'):
    """อ่าน xlsx จากผลลัพธ์ของ ipynbองเพื่อน"""
    data = []
    wb = xl.load_workbook(filename = fname)
    print(wb.sheetnames)
    s = wb['Sheet1']
    c = 'ACBDEFGHIJ'
    for i in range(1,21):
        data.append( ([s[e+str(i)].value for e in c]))
    convert2numpy(data)

def convert2numpy(data):
    """แปลง list น list เป็น numpy.ndarray"""
    datanp = np.array(data)
    save(datanp)

def save(datanp):
    np.save('result2.npy',datanp)
def load():
    Npdata = np.load('result2.npy')
    print(Npdata)
# readxlsx()
load()
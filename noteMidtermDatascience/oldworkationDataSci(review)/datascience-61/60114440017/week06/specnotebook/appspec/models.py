from django.db import models

# Create your models here.
class spec(models.Model):
    id = models.AutoField(primary_key=True) #?
    name = models.TextField()
    price = models.IntegerField()
    brand = models.CharField(max_length=100,default=' ')
    series =  models.CharField(max_length=100,default=' ')
    CPU = models.CharField(max_length=100,default=' ')
    GPU = models.CharField(max_length=100,default=' ')
    display = models.CharField(max_length=100,default=' ')
    OS = models.CharField(max_length=100,default=' ')
    images = models.CharField(max_length=300,default=' ')


    def _str_(self):
        return f'{self.id} {self.name} {self.price} {self.brand} {self.series} {self.CPU} {self.GPU} {self.display} {self.OS} {self.images}'
from django.shortcuts import render
from .models import spec

def index(req):
  specnaja = spec.objects.all()
  return render(req, 'appspec/base.html', {'specnaja':specnaja})
import numpy as np
import os
from django.shortcuts import render
from django.http import HttpResponse
from .models import spec
def load(req):
    mm = np.load('result.npy')
    pushdata(mm)
    return render(req, 'appspec/index.html')

def pushdata(data):
    for ii in range(len(data)):    
        x = data[ii]
        name = x[0]
        price = x[2]
        brand = x[1]
        series = x[3]
        CPU = x[4]
        GPU = x[6]
        display = x[7]
        OS = x[8]
        images = x[9]

        detal = spec.objects.create(name=name,price=price,brand=brand,series=series,CPU=CPU,GPU=GPU,display=display,OS=OS,images=images)   
        detal.save()
    print("สำเร็จ")
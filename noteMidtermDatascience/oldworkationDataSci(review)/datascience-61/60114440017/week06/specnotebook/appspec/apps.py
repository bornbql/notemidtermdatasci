from django.apps import AppConfig


class AppspecConfig(AppConfig):
    name = 'appspec'

from django.contrib import admin
from django.urls import path
from book import views,pushdata

urlpatterns = [
    path('', views.index),
    path('admin/', admin.site.urls),
    path('pushdata/',pushdata.load)
]

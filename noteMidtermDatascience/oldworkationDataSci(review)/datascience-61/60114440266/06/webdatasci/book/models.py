from django.db import models

# Create your models here.
class book(models.Model):
    id = models.AutoField(primary_key=True) #?
    name = models.TextField()
    price = models.FloatField()
    saleP = models.FloatField(default="20")
    
    photo1 = models.CharField(max_length=250,default=' ')


    def _str_(self):
        return f'{self.id} {self.name} {self.price} {self.saleP} {self.photo1}'

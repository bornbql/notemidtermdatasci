from django.shortcuts import render
from django.http import HttpResponse
from .models import book

# Create your views here.

def index(req):
    booked = book.objects.all()
    return render(req, 'book/index.html', {'booked':booked})


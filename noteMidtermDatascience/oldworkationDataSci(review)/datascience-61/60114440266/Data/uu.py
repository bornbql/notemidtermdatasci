#!/usr/bin/env python3
import openpyxl as xl
import numpy as np
def readxlsx(fname = 'ProductS.xlsx'):
    """อ่าน xlsx จากผลลัพธ์ของ ipynbองเพื่อน"""
    data = []
    wb = xl.load_workbook(filename = fname)
    print(wb.sheetnames)
    s = wb['Sheet1']
    col = 'AB'
    for i in range(1,50):
        data.append( ([s[e+str(i)].value for e in col]))
    convert2numpy(data)
def convert2numpy(data):
    """แปลง list น list เป็น numpy.ndarray"""
    datanp = np.array(data)
    save(datanp)

def save(datanp):
    np.save('result.npy',datanp)
def load():
    Npdata = np.load('result.npy')
    print(Npdata)
#readxlsx()
load()
import scrapy

class PagesSpider(scrapy.Spider):

    min_number_of_baan = 5000

    name = "pages"

    url = 'https://baan.kaidee.com/c15-realestate-home/'

    def start_requests(self):
        self.columns = { 'หมายเลขประกาศ':0, 'ราคา':1, 'lat':2, 'lng':3, 'เนื้อที่':4, 'ห้องนอน':5, 'ห้องน้ำ':6 }
        self.result = []
        yield scrapy.Request(url=self.url, callback=self.parse_page)

    def parse_page(self, response):
        # extract page
        li = response.xpath("//li[contains(@class, 'member-ads-preview')]/a[@itemprop='url']/@href").extract()
        img = response.xpath("//li[contains(@class, 'member-ads-preview')]/a/figure/img[@itemprop='image']/@src").extract()
        self.result.extend([ f'{x},{y}' for x,y in zip(li,img) ])
        next_page = response.xpath("//div[contains(@class,'pagination')]/a[contains(@class, 'nextPage')]/@href").extract()
        print(f'len(result) -> {len(self.result)}')
        if next_page and len(self.result) < self.min_number_of_baan:
            link = next_page[0]
            page = int(link.split('-')[-1]) + 1
            self.url = 'https://baan.kaidee.com/c15-realestate-home'+link[:link.rfind('-')] + f'-{page}'
            print(f'nextPage url: "{self.url}"')
            yield scrapy.Request(url=self.url, callback=self.parse_page)
        else:
            with open(f'baan-links.txt', 'w') as f:
                f.writelines('\n'.join(self.result[:self.min_number_of_baan]))
        return self.result

    def print(self):
        with open(f'result.csv', 'w') as f:
            #for v in zip(range(len(self.all_ads)), self.all_ads.values()):
            for v in self.all_ads.values():
               f.writelines(','.join([str(e) for e in v])+'\n')


import scrapy
import json
import pprint
import re

class PagesSpider(scrapy.Spider):

    min_number_of_baan = 4000
    links = 0
    linkfname = 'baan-links.txt'
    output = 'baans-4000.csv'
    name = "baans"
    pp = pprint.PrettyPrinter(width=81, compact=True)
    skipped = 0

    def start_requests(self):
        self.result = []
        self.products = []
        with open(self.linkfname, 'r') as f:
            for line in f.readlines():
                a = line.strip().split(',')
                self.products.append([f'https://baan.kaidee.com{a[0]}', a[1]])

        '''
        url = self.urls[0]
        yield scrapy.Request(url=self.urls[0], callback=self.parse)
        '''
        for product in self.products:
            #yield scrapy.Request(url=product[0], callback=self.parse)
            req = scrapy.Request(url=product[0], callback=self.parse)
            req.meta['img'] = product[1]
            yield req

    def parse(self, response):
        self.links += 1
        # extracting property
        img = response.meta['img']
        ldjson = response.xpath("//article[@class='product']/div[@class='product-active']/script[@type='application/ld+json']/text()").extract_first()
        if not ldjson:
            return None

        j = json.loads(ldjson)
        #self.pp.pprint(j)

        address = response.xpath("//footer[@class='address']/div/address[contains(@class,'_pab')]/span/text()").extract()
        #print(address)

        xtras = response.xpath("//div[contains(@class,'main-information')]/div[@id='ad-detail']/div/ul[contains(@class,'extra-attrs')]/li/b/text()").extract()

        #print('####### xtras#######')
        #print(xtras)

        # skipped 'ไร่'
        if 'ไร่' in xtras[0]:
            return None

        ixtras = [] # int(re.match(r"[0-9]+", x).group()) for x in xtras ]
        for i in range(len(xtras)):
            r = re.match(r"[0-9]+", xtras[i])
            if r:
                ixtras.append(int(r.group()))
            else:
                ixtras.append(0)

        # แปลงจาก ตารางเมตร เป็น ตารางวา
        if 'ตารางเมตร' in xtras[0]: ixtras[0] /= 4
        r = [
            j['url'].split('-')[-1], # id
            int(j['offers']['price']), # price
            address[0],  # district
            address[-1], # province
            0, # area
            0, # bedrooms
            0  # restrooms
        ]
        for i in range(len(ixtras)):
            r[4+i] = ixtras[i]

        r.append(img)
        self.result.append(r)

        #print(f'{self.links}/{len(self.urls)}')
        if len(self.result) == self.min_number_of_baan:
            self.save()
        return None

    def save(self):
        self.columns = { 'id':0, 'price':1, 'district':2, 'province':3, 'area':4, 'bedrooms':5, 'restrooms':6 }
        columns = [ 'id', 'price', 'district', 'province', 'area', 'bedrooms', 'restrooms', 'image' ]
        with open(self.output, 'w') as f:
            f.writelines(','.join(columns) + '\n')
            for baan in self.result:
                f.writelines(','.join([str(x) for x in baan]) + '\n')
            f.close()

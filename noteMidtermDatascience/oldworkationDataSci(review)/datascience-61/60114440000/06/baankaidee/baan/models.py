from django.db import models

# Create your models here.
# id,price,district,province,area,bedrooms,restrooms
# 344508017,3800000,เมืองอุดรธานี,อุดรธานี,67,3,2

# ref: https://docs.djangoproject.com/en/2.1/ref/models/fields/

class Baan(models.Model):
    id = models.CharField(max_length=100, primary_key=True) #?
    price = models.FloatField()
    district = models.TextField()
    province = models.TextField()
    area = models.FloatField()
    bedrooms = models.IntegerField()
    restrooms = models.IntegerField()

    def __str__(self):
      return f'{self.id} {self.district} {self.province}'
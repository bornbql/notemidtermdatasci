#!/usr/bin/python3
# -- coding: utf-8 --
import pandas as pd

df = pd.read_csv('baans-4000.csv')
chiang_rai = df[(df.area > 0) & (df.province=='เชียงราย')]
dist = chiang_rai.groupby('district').mean()
showdt = pd.DataFrame(dist.price/dist.area,columns=['price']).sort_values(by='price',ascending=False)

print(showdt.reset_index())
